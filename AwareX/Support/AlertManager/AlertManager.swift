//
//  AlertManager.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation
import UIKit

class AlertManager {
    static let shared = AlertManager()
    
    func AlertAction(vm : MovieVM,vc : UIViewController,msg : String = .kEmpty,typeOfAlert : TypeOfAlert ,handler: @escaping (()->Void) ) {
        let alertController = UIAlertController(title: msg, message: .kEmpty, preferredStyle: .actionSheet)
        if typeOfAlert.rawValue == TypeOfAlert.filter.rawValue {
            vm.movie?.forEach({ Movie in
                let genreAction = UIAlertAction(title: Movie.genre, style: .default) { action in
                    vm.sortAndFilter(isFilter: true,genre: Movie.genre ?? .kEmpty)
                    handler()
                }
                alertController.addAction(genreAction)
            })
        }else if typeOfAlert.rawValue == TypeOfAlert.year.rawValue || typeOfAlert.rawValue == TypeOfAlert.rate.rawValue {
            let ascendingAction = UIAlertAction(title: .kAscending, style: .default) { action in
                switch typeOfAlert.rawValue {
                case TypeOfAlert.year.rawValue:
                    vm.sortAndFilter(isAscending: true,isYear: true)
                case TypeOfAlert.rate.rawValue:
                    vm.sortAndFilter(isAscending: true,isYear: false)
                default:break
                }
                handler()
            }
            let descendingAction = UIAlertAction(title: .kDescending, style: .default) { action in
                switch typeOfAlert.rawValue {
                case TypeOfAlert.year.rawValue:
                    vm.sortAndFilter(isAscending: false,isYear: true)
                case TypeOfAlert.rate.rawValue:
                    vm.sortAndFilter(isAscending: false,isYear: true)
                default:break
                }
                handler()
            }
            alertController.addAction(ascendingAction)
            alertController.addAction(descendingAction)
        }
        let clearAction = UIAlertAction(title: .kClear, style: .default) { action in
            vm.clearTheSortOrFilter()
            handler()
        }
        let defaultAction = UIAlertAction(title: .kCancel, style: .destructive) { action in}
        alertController.addAction(clearAction)
        alertController.addAction(defaultAction)
        vc.present(alertController, animated: true, completion: nil)
    }
}
