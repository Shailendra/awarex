//
//  Extension+Sting.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation

extension String {
    
    static let kEmpty       = ""
    static let kClear       = "Clear"
    static let kCancel      = "Cancel"
    static let kAscending   = "Ascending"
    static let kDescending  = "Descending"
    static let kOneOption   = "Please choose one option"
    static let kYearOfMovie = "Please sort year of Movie"
    static let kRateOfMovie = "Please sort rating of Movie"
    static let kName        = "Name: "
    static let kYear        = "Year: "
    static let kRating      = "Rating: "
    static let kGenre       = "Genre: "
    static let kPlot        = "Plot: "
    static let kForResource = "movies"
    static let kOfType      = "json"
}
