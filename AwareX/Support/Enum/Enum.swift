//
//  Enum.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation

enum TypeOfAlert : String {
    case year
    case rate
    case filter
}
