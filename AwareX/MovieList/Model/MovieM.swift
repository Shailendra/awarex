//
//  Movies.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation

struct Movie : Codable {
    
    let title       : String?
    let year        : String?
    let genre       : String?
    let plot        : String?
    let imdbRating  : String?
    let movieImages : [String]?
    
    enum CodingKeys: String, CodingKey {
        
        case title       = "Title"
        case year        = "Year"
        case genre       = "Genre"
        case plot        = "Plot"
        case imdbRating  = "imdbRating"
        case movieImages = "Images"
    }
    
    init(from decoder: Decoder) throws {
        let values       = try decoder.container(keyedBy: CodingKeys.self)
        self.title       = try values.decodeIfPresent(String.self, forKey: .title)
        self.year        = try values.decodeIfPresent(String.self, forKey: .year)
        self.genre       = try values.decodeIfPresent(String.self, forKey: .genre)
        self.plot        = try values.decodeIfPresent(String.self, forKey: .plot)
        self.imdbRating  = try values.decodeIfPresent(String.self, forKey: .imdbRating)
        self.movieImages = try values.decodeIfPresent([String].self, forKey: .movieImages)
    }
}
