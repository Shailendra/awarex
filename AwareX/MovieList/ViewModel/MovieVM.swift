//
//  MovieVM.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation

class MovieVM : NSObject {
    
    //MARK: - PROPERTIES
    var movie             : [Movie]!
    var isSortOrFilter    : Bool = false
    var sortOrFilterMovie = [Movie]()
    
    override init() {
        super.init()
        self.getMovieList()
    }
    
    //MARK: - GET MOVIES LIST
    private func getMovieList(){
        do {
            if let bundlePath = Bundle.main.path(forResource: .kForResource, ofType: .kOfType),
               let jsonData   = try String(contentsOfFile: bundlePath).data(using: .utf8) {
               self.movie     = try JSONDecoder().decode([Movie].self, from: jsonData)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //MARK: - THIS FUNCTION FOR FILTER AND SORT OF THE MOVIES
    func sortAndFilter(isAscending : Bool = false ,isYear:Bool = false,isFilter : Bool = false,genre: String = .kEmpty){
        self.isSortOrFilter = true
        if isFilter {
            self.sortOrFilterMovie = self.movie.filter({ movie in
                movie.genre ?? .kEmpty == genre
            })
        }else{
            if isAscending {
                self.sortOrFilterMovie = self.movie.sorted(by: { m, m1 in
                    if isYear {
                        m.year ?? .kEmpty < m1.year ?? .kEmpty
                    }else{
                        m.imdbRating ?? .kEmpty < m1.imdbRating ?? .kEmpty
                    }
                })
            }else{
                self.sortOrFilterMovie = self.movie.sorted(by: { m, m1 in
                    if isYear {
                        m.year ?? .kEmpty > m1.year ?? .kEmpty
                    }else{
                        m.imdbRating ?? .kEmpty > m1.imdbRating ?? .kEmpty
                    }
                })
            }
        }
    }
    
    //MARK: - THIS FUNCTION CLEAR THE FILTER OR SORT OF THE MOVIES
    func clearTheSortOrFilter() {
        self.isSortOrFilter = false
    }
}
