//
//  MovieCell.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import UIKit


class MovieCell: UITableViewCell {

    //MARK: - PROPERIES
    @IBOutlet weak var titleLabel       : UILabel!
    @IBOutlet weak var yearLabel        : UILabel!
    @IBOutlet weak var iMdbRatingLabel  : UILabel!
    @IBOutlet weak var genreLabel       : UILabel!
    @IBOutlet weak var plotLabel        : UILabel!
    @IBOutlet var carousel: ZKCarousel? = ZKCarousel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.carousel?.layer.cornerRadius = 8.0
        self.carousel?.layer.borderWidth  = 2.0
        self.carousel?.layer.borderColor  = UIColor.gray.cgColor
        self.carousel?.clipsToBounds      = true
    }
}
