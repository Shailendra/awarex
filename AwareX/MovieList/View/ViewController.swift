//
//  ViewController.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import UIKit

class ViewController: UIViewController {
    
    //MARK: - PROPERTIES
    private var vm                    : MovieVM!
    private var dataSource            : MovieTableViewDataSource<MovieCell,Movie>!
    @IBOutlet weak var movieTableView : UITableView!
    
    //MARK: - LIFE CYCLE OF VIEW CONTROLLER
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movieTableView.register(UINib(nibName: IdentifierName.kMovieCell, bundle: nil), forCellReuseIdentifier: IdentifierName.kMovieCell)
        self.allocViewModel()
    }
    
    //MARK: - ALLOC THE VIEW MODEL
    func allocViewModel(){
        self.vm = MovieVM()
        if self.vm.movie != nil {
            self.updateDataSource()
        }
    }
    
    //MARK: - THIS FUNCTION WILL WORK FOR UPDATE THE TABLEVIEW
    func updateDataSource(){
        self.dataSource = MovieTableViewDataSource(identifier: IdentifierName.kMovieCell, movies:self.vm.isSortOrFilter ? self.vm.sortOrFilterMovie : self.vm.movie, configureCell: { cell, vm in
            cell.titleLabel.text      = .kName   + (vm.title      ?? .kEmpty)
            cell.yearLabel.text       = .kYear   + (vm.year       ?? .kEmpty)
            cell.iMdbRatingLabel.text = .kRating + (vm.imdbRating ?? .kEmpty)
            cell.genreLabel.text      = .kGenre  + (vm.genre      ?? .kEmpty)
            cell.plotLabel.text       = .kPlot   + (vm.plot       ?? .kEmpty)
            self.loadTheMovieImages(vm: vm, cell: cell)
        })
        DispatchQueue.main.async {
            self.movieTableView.dataSource = self.dataSource
            self.movieTableView.reloadData()
        }
    }
    
    //MARK: - THIS FUNCTION FOR LOAD THE MOVIES IMAGES
    func loadTheMovieImages(vm:Movie,cell:MovieCell){
        var slideArray = [ZKCarouselSlide]()
        DispatchQueue.global(qos: .background).async {
            do{
                for i in 0..<(vm.movieImages?.count ?? 0)  {
                    let data = try Data.init(contentsOf: URL.init(string:vm.movieImages?[i] ?? .kEmpty)!)
                    DispatchQueue.main.async {
                        let image: UIImage = UIImage(data: data)!
                        slideArray.append(ZKCarouselSlide(image: image))
                    }
                }
                cell.carousel?.slides = slideArray
            }
            catch {print(error.localizedDescription.description)}
        }
    }
    //MARK: - THIS FUNCTION FOR FILTER AND SORT OF THE MOVIES
    @IBAction func filterOrSortAction(sender:UIBarButtonItem){
        var typeOfAlert  = TypeOfAlert.year
        var msg : String = .kEmpty
        if sender.tag == 0 {       typeOfAlert = .year; msg   = .kYearOfMovie
        }else if sender.tag == 1 { typeOfAlert = .rate; msg   = .kRateOfMovie
        }else if sender.tag == 2 { typeOfAlert = .filter; msg = .kOneOption }
        AlertManager.shared.AlertAction(vm: self.vm, vc: self,msg: msg,typeOfAlert: typeOfAlert) {
            self.updateDataSource()
        }
    }
}
