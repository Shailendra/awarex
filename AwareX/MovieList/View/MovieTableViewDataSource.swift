//
//  MovieTableViewDataSource.swift
//  AwareX
//
//  Created by Shailendra Kumar Gupta on 20/01/24.
//

import Foundation
import UIKit

class MovieTableViewDataSource<MovieCell : UITableViewCell,T> : NSObject, UITableViewDataSource {
    
    private var identifier : String!
    private var movies     : [T]!
    var configureCell      : (MovieCell, T) -> () = {_,_ in }
    
    init(identifier : String, movies : [T], configureCell : @escaping (MovieCell, T) -> ()) {
        self.identifier    = identifier
        self.movies        = movies
        self.configureCell = configureCell
    }
    
    //MARK: - TABLEVIEW DATASOURCE METHOD
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier, for: indexPath) as! MovieCell
        let item = self.movies[indexPath.row]
        self.configureCell(cell, item)
        return cell
    }
}
